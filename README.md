membuat uuid untuk database tanpa menggunakan library  
ada dua cara
- Menggunakan trait (bisa diliat di model blog, cocok untuk penggunaan global)
- langsung digunakan di model (bisa diliat di model user untuk contoh)


**catatan**  
pada method boot di model harus ditambahkan `parent::boot();` agar tidak error.  
pada trait pun method tidak boleh menggunakan nama boot().